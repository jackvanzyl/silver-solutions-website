$(document).ready(function() {

  // ==================================================
  // Navbar
  // --------------------------------------------------

  $('#menu-gear').click(function(event) {
    // Close any of the other dropdown panels that may be open.
    $('#search-dropdown').removeClass('open');

    // Toggle this dropdown menu.
    $('#navbar-dropdown').toggleClass('open');
    event.preventDefault();
  });

  // $('#menu-gear').hover(
  //   function() {
  //     $('#navbar-dropdown').addClass('open');
  //   }, function() {
  //     $('#navbar-dropdown').removeClass('open');
  //   }
  // );

  $('#menu-search').click(function(event) {
    // Close any of the other dropdown panels that may be open.
    $('#navbar-dropdown').removeClass('open');

    // Toggle this dropdown menu.
    $('#search-dropdown').toggleClass('open');
    event.preventDefault();
  });

  $('#menu-cart').click(function(event) {
    event.preventDefault();
    console.log('@todo: Cart...');
  });

  // --------------------------------------------------

});
