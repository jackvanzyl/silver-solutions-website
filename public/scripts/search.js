$(document).ready(function() {

  // ==================================================
  // Google Custom Search
  // https://developers.google.com/custom-search/v1
  // https://programmablesearchengine.google.com/
  // --------------------------------------------------

  const params = new URLSearchParams(location.search);
  const searchTerm = params.get('search-term');
  $('#search-term-heading').text('Search for "' + searchTerm + '"');

  $.get('https://www.googleapis.com/customsearch/v1?key=' + customSearchKey + '&cx=' + customSearchEngineId + '&q=' + encodeURIComponent(searchTerm), function(result) {
    if (!result.items || !result.items.length) {
      $('#search-results').html('<li><span id="search-results-item-header">Nothing found.</span></li>');
    } else {
      $('#search-results').html('');
      $.each(result.items, function(counter, item) {
        $('#search-results').append('<li><span><span id="search-results-item-header"><a href="' + item['link'] + '">' + item['htmlTitle'] + '</a></span><br>' + item['htmlSnippet'] + '<br><a href="' + item['link'] + '">' + item['htmlFormattedUrl'] + '</a></span></li>')
      });
    }
  });
  
  // --------------------------------------------------

});
